﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ViewSelectLevel: WindowViewBase
{
    [SerializeField]
    private Button _buttonEasy;

    [SerializeField]
    private Button _buttonMedium;

    [SerializeField]
    private Button _buttonHard;

    void Start()
    {
        _buttonEasy.onClick.AddListener(EasyFunction);

        _buttonMedium.onClick.AddListener(MediumFunction);

        _buttonHard.onClick.AddListener(HardFunction);

        buttonClose.onClick.AddListener(Close);
    }

    private void EasyFunction()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.SelectLevel, this);
 
        SceneManager.LoadScene("Game");

        UnityPoolManager.Instance.ClearPool();

        ProfilePlayer.GetLevel = 1;
        
    }
    private void MediumFunction()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.SelectLevel, this);

        SceneManager.LoadScene("Game");

        UnityPoolManager.Instance.ClearPool();

        ProfilePlayer.GetLevel = 2;

    }
    private void HardFunction()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.SelectLevel, this);
       
        SceneManager.LoadScene("Game");

        UnityPoolManager.Instance.ClearPool();

        ProfilePlayer.GetLevel = 3;


    }

    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.SelectLevel, this);
    }
        
}
