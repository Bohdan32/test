﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ViewResult : WindowViewBase 
{
    [SerializeField]
    private Text _textNameResult;

    [SerializeField]
    private Text _textValueResult;

	void Start () 
    {
        buttonClose.onClick.AddListener(Close);
	}
	
    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.SelectLevel, this);

        SceneManager.LoadScene("Menu");

        UnityPoolManager.Instance.ClearPool();
    }
	    
    public void Construct(int value)
    {
        switch (value)
        {
            case 1:
                ProfilePlayer.EditWin(1);
                ProfileBot.EditLose(1);
                _textNameResult.text = "Поздравляю, ты выиграл";
                _textValueResult.text = "Победы: " + ProfilePlayer.GetWin.ToString();
                break;

            case 2:
                ProfilePlayer.EditLose(1);
                ProfileBot.EditWin(1);
                _textNameResult.text = "К сожалению ты проиграл";
                _textValueResult.text = "Пригрыши: " + ProfilePlayer.GetLose.ToString();
                break;

            case 0:
                ProfilePlayer.EditDraws(1);
                ProfileBot.EditDraws(1);
                _textNameResult.text = "Вы сыграли в ничью";
                _textValueResult.text = "Ничьи: "+ProfilePlayer.GetDraws.ToString();
                break;
        }
    }
}
